iosNode = 'mac-builder-xcode9'

def fullCleanAndCheckout() {
  stage('Checkout & Setup') {
      checkout scm
      echo "My branch is: ${env.BRANCH_NAME}"
      echo "Install dependencies"
      cmd 'export'
      cmd '/usr/local/bin/bundle install --path vendor/bundle'
  }
}

def clean() {
  stage('Clean') {
      cmd "/usr/local/bin/bundle exec fastlane clean"
  }
}

def buildEnterprise() {
  try {
    stage('Build') {
        parallel(
            "Build Dev" : { cmd "/usr/local/bin/bundle exec fastlane enterprise" }
        )
    }

    def devIPAPath = 'build/Antispoof.ipa'
    def devDSYMPath = 'build/Antispoof.app.dSYM.zip'

    stage('Archive') {
        parallel(
            "Archive Dev": {
                archiveArtifacts artifacts: devIPAPath, fingerprint: true
                archiveArtifacts artifacts: devDSYMPath, fingerprint: true
            }
        )
    }

    stage('Distribute') {
        parallel(
            "Distribute Dev": {
                withCredentials([[$class: 'StringBinding', credentialsId: 'ANTISPOOF_APP_HOCKEYAPP_UPLOAD_TOKEN', variable: 'HOCKEYAPP_UPLOAD_TOKEN']]) {
                    withCredentials([[$class: 'StringBinding', credentialsId: 'ANTISPOOF_APP_HOCKEYAPP_ID', variable: 'HOCKEYAPP_ID']]) {
                        sendToHockeyApp(devIPAPath, devDSYMPath, HOCKEYAPP_ID, HOCKEYAPP_UPLOAD_TOKEN)
                    }
                }
            }
        )
    }

  } catch (InterruptedException x) {
          currentBuild.result = 'ABORTED'
  } catch (e) {
      currentBuild.result = 'FAILURE'
      handleException();
      throw e
  }
}

def performEnterpriseDistributionWork() {
  try {
    node(iosNode) {
      try {
        fullCleanAndCheckout()
        clean()
        buildEnterprise()
      } finally {
        collectReports()
      }
    }
  } catch (InterruptedException x) {
          currentBuild.result = 'ABORTED'
  } catch (e) {
      currentBuild.result = 'FAILURE'
      handleException();
      throw e
  }
}

//MAIN BLOCK

if ("${env.BRANCH_NAME}" ==~ /master/) {
    echo "Performing stages after merge to branch ${env.BRANCH_NAME}"
    performEnterpriseDistributionWork()
}

//MAIN BLOCK END

def collectReports() {
    stage('Archive Reports') {
        step([$class: 'XUnitBuilder',
                thresholds: [[$class: 'FailedThreshold', unstableThreshold: '1']],
                tools: [[$class: 'JUnitType', pattern: 'build/reports_output/**/*.junit']]])
        archiveArtifacts artifacts: 'build/reports_output/**/*', fingerprint: true
    }
}

def sendToHockeyApp(String ipaPath, String dsymPath, String hockeyAppApiId, String uploadToken) {
    def notes = "URL: ${env.BUILD_URL}\nChangelog:\n" + getChangeString()
    def sendCommand = "curl https://rink.hockeyapp.net/api/2/apps/${hockeyAppApiId}/app_versions/upload" +
            "    -F \"status=2\"" +
            "    -F \"notify=1\"" +
            "    -F \"mandatory=0\"" +
            "    -F \"ipa=@${ipaPath}\"" +
            "    -F \"dsym=@${dsymPath}\"" +
            "    -F \"notes=${notes}\"" +
            "    -H \"X-HockeyAppToken: ${uploadToken}\""
    echo sendCommand
    cmd sendCommand
}

@NonCPS
def getChangeString() {
    def jiraKeyPattern = /CYC-\d*/
    MAX_MSG_LEN = 100
    def changeString = ""

    echo "Gathering SCM changes"
    def changeLogSets = currentBuild.rawBuild.changeSets
    for (int i = 0; i < changeLogSets.size(); i++) {
            def entries = changeLogSets[i].items
        for (int j = 0; j < entries.length; j++) {
            def entry = entries[j]
            def jiraKeys = (entry.comment =~ /$jiraKeyPattern/)
            echo "MSG" + entry.msg
            echo "Issues found " + jiraKeys.count
            changeString += "- "
            for (int k = 0; k < jiraKeys.count; k++) {
                changeString += jiraKeys[k] + " "
            }
            truncated_msg = entry.msg.take(MAX_MSG_LEN).replace("\"", "").replace("\'", "");
            changeString += "${truncated_msg} [${entry.author}]\n"
        }
    }

    if (!changeString) {
        changeString = "- No new changes"
    }
    return changeString
}

def cmd(String shellCommand) {
    wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'XTerm']) {
        sh shellCommand
    }
}
