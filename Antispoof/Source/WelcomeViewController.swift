//
//  HomeViewController.swift
//  Antispoof
//
//  Created by Przemyslaw Probola on 10/04/2018.
//  Copyright © 2018 YND. All rights reserved.
//

import Foundation
import UIKit

final class WelcomeViewController: UIViewController {
    
    @IBOutlet weak var dismissButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .`default`)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        dismissButton.layer.cornerRadius = 4.0
        dismissButton.layer.masksToBounds = true
    }
}
