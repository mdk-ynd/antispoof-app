//
//  UserSettings.swift
//  Antispoof
//
//  Created by Przemyslaw Probola on 10/04/2018.
//  Copyright © 2018 YND. All rights reserved.
//

import Foundation

struct UserSettings {
    
    private static let onboardFladKey = "com.ynd.onboarding"
    static var isUserOnboarded: Bool {
        return UserDefaults.standard.bool(forKey: onboardFladKey)
    }
    
    static func setUserOnboardedSetting() {

        let defaults = UserDefaults.standard
        defaults.set(true, forKey: onboardFladKey)
        defaults.synchronize()
    }
}
