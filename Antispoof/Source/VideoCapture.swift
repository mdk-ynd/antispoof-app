import UIKit
import AVFoundation
import CoreVideo

public protocol VideoCaptureDelegate: class {
    func videoCapture(_ capture: VideoCapture, didCaptureVideoFrame: CVPixelBuffer?, timestamp: CMTime)
}

public class VideoCapture: NSObject {
    
    private let faceBoundingLayer = CAShapeLayer()
    public var previewLayer: AVCaptureVideoPreviewLayer?
    public weak var delegate: VideoCaptureDelegate?
    public var fps = 15
    
    let captureSession = AVCaptureSession()
    let videoOutput = AVCaptureVideoDataOutput()
    let queue = DispatchQueue(label: "com.ynd.camera-queue")
    
    var lastTimestamp = CMTime()
    
    public override init() {
        super.init()
        
        
    }
    
    public func setUp(sessionPreset: AVCaptureSession.Preset = .medium,
                      completion: @escaping (Bool) -> Void) {
        queue.async {
            let success = self.setUpCamera(sessionPreset: sessionPreset)
            DispatchQueue.main.async {
                completion(success)
            }
        }
    }
    
    func setUpCamera(sessionPreset: AVCaptureSession.Preset) -> Bool {
        captureSession.beginConfiguration()
        captureSession.sessionPreset = sessionPreset
        
        guard let captureDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: AVCaptureDevice.Position.front) else {
            print("Error: no video devices available")
            return false
        }
        
        guard let videoInput = try? AVCaptureDeviceInput(device: captureDevice) else {
            print("Error: could not create AVCaptureDeviceInput")
            return false
        }
        
        if captureSession.canAddInput(videoInput) {
            captureSession.addInput(videoInput)
        }
        
        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        previewLayer.connection?.videoOrientation = .portrait
        self.previewLayer = previewLayer
        setupFaceBoundingLayer()
        
        let settings: [String : Any] = [
            kCVPixelBufferPixelFormatTypeKey as String: NSNumber(value: kCVPixelFormatType_32BGRA),
            ]
        
        videoOutput.videoSettings = settings
        videoOutput.alwaysDiscardsLateVideoFrames = true
        videoOutput.setSampleBufferDelegate(self, queue: queue)
        if captureSession.canAddOutput(videoOutput) {
            captureSession.addOutput(videoOutput)
        }
        
        // We want the buffers to be in portrait orientation otherwise they are
        // rotated by 90 degrees. Need to set this _after_ addOutput()!
        videoOutput.connection(with: AVMediaType.video)?.videoOrientation = .portrait
        
        captureSession.commitConfiguration()
        return true
    }
    
    public func start() {
        if !captureSession.isRunning {
            captureSession.startRunning()
        }
    }
    
    public func stop() {
        if captureSession.isRunning {
            captureSession.stopRunning()
        }
    }
}

extension VideoCapture: AVCaptureVideoDataOutputSampleBufferDelegate {
    public func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        // Because lowering the capture device's FPS looks ugly in the preview,
        // we capture at full speed but only call the delegate at its desired
        // framerate.
        let timestamp = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)
        let deltaTime = timestamp - lastTimestamp
        if deltaTime >= CMTimeMake(1, Int32(fps)) {
            lastTimestamp = timestamp
            let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)
            delegate?.videoCapture(self, didCaptureVideoFrame: imageBuffer, timestamp: timestamp)
        }
    }
    
    public func captureOutput(_ output: AVCaptureOutput, didDrop sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        //print("dropped frame")
    }
}

// MARK: Face bounding box
extension VideoCapture {
    
    private func setupFaceBoundingLayer() {
        
        faceBoundingLayer.borderColor = UIColor(displayP3Red: 62.0 / 255.0, green: 210 / 155.0, blue: 209 / 255.0, alpha:0.9).cgColor
        faceBoundingLayer.borderWidth = 2.0
        faceBoundingLayer.opacity = 0.0
        previewLayer?.addSublayer(faceBoundingLayer)
    }
    
    func showFaceBounding(rect: CGRect, sourceImageSize: CGSize) {
        
        guard let previewSize = previewLayer?.bounds.size, rect.size.width > 0, rect.size.height > 0 else {
            return
        }
        let scaleX: CGFloat = previewSize.width / sourceImageSize.width
        let scaleY: CGFloat = previewSize.height / sourceImageSize.height
        
        let width: CGFloat = rect.size.width * scaleX
        let height: CGFloat = rect.size.height * scaleY
        
        var x: CGFloat = rect.origin.x * scaleX
        let y: CGFloat = rect.origin.y * scaleY
        
        x = previewSize.width - x - width//Mirror x axis
        let finalRect = CGRect(x: x, y: y, width: width, height: height)
        
        if faceBoundingLayer.frame.size.width == 0 {
            self.faceBoundingLayer.frame = finalRect
        }
        UIView.animate(withDuration: 0.15, animations: {
            self.faceBoundingLayer.opacity = 1.0
            self.faceBoundingLayer.frame = finalRect
        }) { (finished) in
            
        }
    }
    
    func hideFaceBoundingRect() {
        
        UIView.animate(withDuration: 0.25) {
            self.faceBoundingLayer.opacity = 0.0
        }
    }
}
