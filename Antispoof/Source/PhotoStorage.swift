//
//  PhotoStorage.swift
//  Antispoof
//
//  Created by Przemyslaw Probola on 11/04/2018.
//  Copyright © 2018 YND. All rights reserved.
//

import UIKit
import Firebase

class PhotoStorage {
    
    private var storageRef: StorageReference!
    private var isAuthenticated = false
    init() {
        
    }
    
    func configure() {
        storageRef = Storage.storage().reference()
        loginToStorage()
    }
    
    private func loginToStorage() {
        if Auth.auth().currentUser == nil {
            Auth.auth().signInAnonymously(completion: { (user: User?, error: Error?) in
                if let error = error {
                    print("Storage - Auth error \(error)")
                    self.isAuthenticated = false
                } else {
                    self.isAuthenticated = true
                }
            })
        }
    }
    
    func uploadPhoto(_ imageData: Data, fileName: String, customMetadata: [String: String]) {
        
        
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        metadata.customMetadata = customMetadata
        let filePath = Auth.auth().currentUser!.uid + "/\(fileName)"
        storageRef.child(filePath).putData(imageData, metadata: metadata) { (storedMetadata, error) in
            
            if let error = error {
                print("Storage - uplaod photo error \(error)")
            } else {
                let url = storedMetadata?.downloadURL()?.absoluteString ?? "not found"
                print("Storage - upload success image url = \(url)")
            }
        }
    }
}


//let filePath = Auth.auth().currentUser!.uid +
//"/\(Int(Date.timeIntervalSinceReferenceDate * 1000))/\(imageFile!.lastPathComponent)"
//// [START uploadimage]
//self.storageRef.child(filePath)
//    .putFile(from: imageFile!, metadata: nil) { (metadata, error) in
//        if let error = error {
//            print("Error uploading: \(error)")
//            self.urlTextView.text = "Upload Failed"
//            return
//        }
//        self.uploadSuccess(metadata!, storagePath: filePath)
//}
