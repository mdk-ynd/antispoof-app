//
//  Analytics.swift
//  Antispoof
//
//  Created by Przemyslaw Probola on 10/04/2018.
//  Copyright © 2018 YND. All rights reserved.
//

import Foundation
import Firebase

struct AnalitycsTracker {
    
    static func trackSpoofEvent(result: String, score: String) {
        
        let name = "SpoofingProcessingDone"
        let parameters: [String: Any] = [
                                        AnalyticsParameterItemName: name,
                                        "result": result,
                                        "avg_score": score
                                        ]
        Analytics.logEvent(name, parameters: parameters)
    }
}
