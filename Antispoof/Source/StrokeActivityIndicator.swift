//
//  StrokeActivityIndicator.swift
//  Antispoof
//
//  Created by Przemyslaw Probola on 09/04/2018.
//  Copyright © 2018 YND. All rights reserved.
//

import UIKit

class StrokeActivityIndicator {
    
    private(set) var isAnimating = false
    
    private let outerCircle = CAShapeLayer()
    private let maskLayer = CAShapeLayer()
    private var strokeColor: UIColor = UIColor(red: 0.45, green: 0.84, blue: 0.86, alpha: 1.0)
    private weak var hostingView: UIView? = nil
    
    convenience init(hostingView: UIView) {
        self.init()
        self.hostingView = hostingView
        
        outerCircle.fillColor = UIColor.clear.cgColor
        outerCircle.strokeColor = strokeColor.cgColor
        outerCircle.lineWidth = 2.0
        hostingView.layer.addSublayer(outerCircle)
        
        setDefaultStrokeState()
    }
    
    func layoutSubviews() {
        
        guard let view = hostingView else {
            return
        }
        outerCircle.frame = view.bounds
        outerCircle.path = UIBezierPath(roundedRect: view.bounds, cornerRadius: 16.0).cgPath
        maskLayer.frame = view.bounds
        maskLayer.path = outerCircle.path
        hostingView?.layer.mask = maskLayer
    }
    
    private func setDefaultStrokeState() {
        
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        
        outerCircle.strokeStart = 0.0
        outerCircle.strokeEnd = 0.0
        
        CATransaction.commit()
    }
    
    private func animateStrokeEndState() {
        
        let endStroke = strokeEndAnimation(to: 1.0)
        let startStroke = strokeStartAnimation(to: 1.0)
        let group = CAAnimationGroup()
        group.animations = [startStroke, endStroke] as [CABasicAnimation]
        group.duration = 0.5
        group.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
        outerCircle.add(group, forKey: "EndState")
        
        outerCircle.strokeStart = 1.0
        outerCircle.strokeEnd = 1.0
        
    }
    
    private func animateEnd() {
        
        CATransaction.begin()
        CATransaction.setAnimationDuration(1.0)
        
        CATransaction.setCompletionBlock { [weak self] in
            self?.animateStart()
        }
        
        let animation1 = strokeEndAnimation(to: 1.0)
        let animation2 = strokeStartAnimation(to: 0.2)
        
        outerCircle.add(animation1, forKey: "End")
        outerCircle.add(animation2, forKey: "Start")
        
        self.outerCircle.strokeStart = 0.6
        self.outerCircle.strokeEnd = 0.95
        
        CATransaction.commit()
    }
    
    private func animateStart() {
        
        CATransaction.begin()
        CATransaction.setAnimationDuration(0.7)
        
        CATransaction.setCompletionBlock {  [weak self] in
            
            self?.setDefaultStrokeState()
            
            if self?.isAnimating == true {
                self?.animateEnd()
            } else {
                
                self?.animateStrokeEndState()
            }
        }
        
        let animation = strokeStartAnimation(to: 1.0)
        outerCircle.add(animation, forKey: "Start")
        self.outerCircle.strokeStart = 1.0
        
        CATransaction.commit()
    }
    
    private func strokeEndAnimation(to value: CGFloat) -> CABasicAnimation {
        
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.toValue = value
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        
        return animation
    }
    
    private func strokeStartAnimation(to value: CGFloat) -> CABasicAnimation {
        
        let animation = CABasicAnimation(keyPath: "strokeStart")
        animation.toValue = value
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        
        return animation
    }
    
    func stopAnimating() {
        isAnimating = false
    }
    
    func startAnimating() {
        if isAnimating {
            return
        }
        isAnimating = true
        animateEnd()
    }
}
