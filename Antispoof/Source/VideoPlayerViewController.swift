//
//  ViewController.swift
//  Antispoof
//
//  Created by Michael Dominic K. on 19/03/2018.
//  Copyright © 2018 YND. All rights reserved.
//

import UIKit
import AVFoundation

class VideoPlayerViewController: UIViewController {

    private let storage = PhotoStorage()
    private var lastCameraProcessedImage = UIImage()
    private weak var welcomeViewController: WelcomeViewController? = nil
    
    @IBOutlet weak var videoPreview: UIView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var statusBackgroundView: UIView!
    @IBOutlet weak var debugResultsTextView: UITextView!
    
    
    var activityIndicator: StrokeActivityIndicator!
    var videoCapture: VideoCapture = VideoCapture()
    let semaphore = DispatchSemaphore(value: 1)
    let spoofingController = SpoofingController()
    var player: AVAudioPlayer!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        activityIndicator = StrokeActivityIndicator(hostingView: statusBackgroundView)
        
        if UserSettings.isUserOnboarded == true {
            setUpCamera()
        } else {
            presentWelcomeView()
        }
        allocAudioPlayer()

        self.debugResultsTextView.alpha = 0.0
        
        storage.configure()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        welcomeViewController?.view.frame = view.bounds
        videoCapture.previewLayer?.frame = videoPreview.bounds
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        activityIndicator.layoutSubviews()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    private func allocAudioPlayer() {
        let path = Bundle.main.path(forResource: "Affirmative", ofType: "mp3")
        let url = URL(fileURLWithPath: path!)
        player = try! AVAudioPlayer(contentsOf: url)
    }

    func setUpCamera() {

        videoCapture.delegate = self
        videoCapture.fps = 25
        videoCapture.setUp(sessionPreset: AVCaptureSession.Preset.high) { success in
            if success {
                // Add the video preview into the UI.
                if let previewLayer = self.videoCapture.previewLayer {
                    self.videoPreview.layer.addSublayer(previewLayer)
                    self.view.setNeedsLayout()
                    self.view.layoutIfNeeded()
                }
                // Once everything is set up, we can start capturing live video.
                self.videoCapture.start()
            }
        }
        spoofingController.delegate = self
        spoofingControllerDidChangeState(newState: .showFace)
    }

    func updateInstructions(currentState state: SpoofingState) {
        switch(state) {
        case .showFace:
            self.stopActivityIndicator()
            self.videoCapture.hideFaceBoundingRect()
            
            self.statusBackgroundView.backgroundColor = UIColor(displayP3Red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
            self.statusLabel.isHidden = true
            self.statusBackgroundView.isHidden = true
            self.debugResultsTextView.isHidden = true
            self.debugResultsTextView.text = ""
        case .facePresented:
            break
        case .moveCloser:
            self.stopActivityIndicator()
            
            self.statusBackgroundView.backgroundColor = UIColor(displayP3Red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
            self.statusLabel.isHidden = false
            self.statusBackgroundView.isHidden = false
            self.statusLabel.text = "Move closer"
            self.debugResultsTextView.isHidden = true
            self.debugResultsTextView.text = ""
            
        case .hold:
            self.startActivityIndicator()
            
            self.statusBackgroundView.backgroundColor = UIColor(displayP3Red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
            self.statusLabel.isHidden = false
            self.statusBackgroundView.isHidden = false
            self.statusLabel.text = "Hold still"
            self.debugResultsTextView.isHidden = true
            self.debugResultsTextView.text = ""
        case .changeLighting:
            self.stopActivityIndicator()
            
            self.statusBackgroundView.backgroundColor = UIColor(displayP3Red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
            self.statusLabel.isHidden = false
            self.statusBackgroundView.isHidden = false
            self.statusLabel.text = "Change lighting"
            self.debugResultsTextView.isHidden = false
        case .inProgress(_):
            self.startActivityIndicator()
            
            self.statusBackgroundView.backgroundColor = UIColor(displayP3Red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
            self.statusLabel.isHidden = false
            self.statusBackgroundView.isHidden = false
            self.statusLabel.text = "Hold still"
            self.debugResultsTextView.isHidden = false
        case .finished(let success):
            self.stopActivityIndicator()
            
            
            if success {
                let result = "Real"
                player.play()
                let generator = UIImpactFeedbackGenerator(style: .light)
                generator.impactOccurred()
                self.statusLabel.isHidden = false
                self.statusBackgroundView.isHidden = false
                self.statusLabel.text = result
                self.statusBackgroundView.backgroundColor = UIColor(displayP3Red: 0.0, green: 144 / 255.0, blue: 81.0 / 255.0, alpha: 1.0)
                self.debugResultsTextView.isHidden = false
                trackSpoofingResultEvent(result)
            } else {
                let result = "Fake"
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
                self.statusLabel.isHidden = false
                self.statusBackgroundView.isHidden = false
                self.statusLabel.text = result
                self.statusBackgroundView.backgroundColor = UIColor(displayP3Red: 217 / 255.0, green: 23.0 / 255.0, blue: 63.0 / 255.0, alpha: 1.0)
                self.debugResultsTextView.isHidden = false
                trackSpoofingResultEvent(result)
            }
        }
    }
}

extension VideoPlayerViewController: VideoCaptureDelegate {
    func videoCapture(_ capture: VideoCapture, didCaptureVideoFrame pixelBuffer: CVPixelBuffer?, timestamp: CMTime) {
        // For debugging.
        //predict(image: UIImage(named: "dog416")!); return

        semaphore.wait()

        if let pixelBuffer = pixelBuffer {
            // For better throughput, perform the prediction on a background queue
            // instead of on the VideoCapture queue. We use the semaphore to block
            // the capture queue and drop frames when Core ML can't keep up.
            self.spoofingController.push(pixelBuffer: pixelBuffer)
        }
    }
}

extension VideoPlayerViewController: SpoofingControllerDelegate {

    func spoofingControllerDidFinishProcessingFrame() {
        semaphore.signal()
        
    }

    func spoofingControllerDidChangeState(newState: SpoofingState) {
        DispatchQueue.main.async {
            print("* \(newState)")
            self.updateInstructions(currentState: newState)
        }
    }

    func spoofingControllerDidGetNewReading(level: Double) {
        DispatchQueue.main.async {
            guard var currentText = self.debugResultsTextView.text else {
                return
            }

            if !currentText.isEmpty {
                currentText = "\(currentText)\n"
            }

            let newValueText = String(format: "%.3f", level)
            self.debugResultsTextView.text = "\(currentText)\(newValueText)"
        }
    }

    func spoofingControllerDidMarkFace(rect: CGRect, sourceImage: UIImage) {
        lastCameraProcessedImage = sourceImage
        
        DispatchQueue.main.async {
            print("RECT: \(rect)")
            self.videoCapture.showFaceBounding(rect: rect, sourceImageSize: self.lastCameraProcessedImage.size)
        }
    }
}

// MARK: ActivityIndicator
extension VideoPlayerViewController {
    private func startActivityIndicator() {
        activityIndicator.startAnimating()
    }
    
    private func stopActivityIndicator() {
        activityIndicator.stopAnimating()
    }
}

// MARK: Present Welcome View
extension VideoPlayerViewController {
    
    func presentWelcomeView() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "WelcomView")
        welcomeViewController = viewController as? WelcomeViewController
        viewController.beginAppearanceTransition(true, animated: false)
        addChildViewController(viewController)
        viewController.view.frame = view.bounds
        view.addSubview(viewController.view)
        viewController.didMove(toParentViewController: self)
        viewController.endAppearanceTransition()
        
        welcomeViewController?.dismissButton?.addTarget(self, action: #selector(dismissWelcomView), for: .touchUpInside)
    }
    
    @objc private func dismissWelcomView() {
        
        UserSettings.setUserOnboardedSetting()
        setUpCamera()
        
        welcomeViewController?.beginAppearanceTransition(false, animated: true)
        UIView.animate(withDuration: 0.25, animations: {
            self.welcomeViewController?.view.alpha = 0.0
        }, completion: { (_) in
            self.welcomeViewController?.willMove(toParentViewController: nil)
            self.welcomeViewController?.view.removeFromSuperview()
            self.welcomeViewController?.removeFromParentViewController()
            self.welcomeViewController?.endAppearanceTransition()
            self.welcomeViewController = nil
        })
    }
}

// MARK: Photo storage
extension VideoPlayerViewController {
    
    func uploadLastCameraPreviewImage(spoofingResult: String, score: String, allReadings: String) {
        
        let faceImage = spoofingController.spoofingDetector.lastDetectionImage
        guard let faceImageData = UIImageJPEGRepresentation(faceImage, 1.0) else {
            return
        }
        
        let metadata = ["result" : spoofingResult, "avgScore" : score, "allReadings": allReadings]
        let timestamp = Int(Date().timeIntervalSince1970)
        let faceFileName = "\(timestamp)_face_\(spoofingResult)"
        storage.uploadPhoto(faceImageData, fileName: faceFileName, customMetadata: metadata)
        
        
        
        let fullImage = lastCameraProcessedImage
        guard let fullImageData = UIImageJPEGRepresentation(fullImage, 0.8) else {
            return
        }
        
        let fullFileName = "\(timestamp)_full_\(spoofingResult)"
        storage.uploadPhoto(fullImageData, fileName: fullFileName, customMetadata: metadata)
        
    }
}

// MARK: Analytics
extension VideoPlayerViewController {
    
    private func trackSpoofingResultEvent(_ result: String) {
        
        let avgScore = "\(self.spoofingController.avgReading)"
        let allReadings = self.spoofingController.readingsArrayString
        let uploadQueue = DispatchQueue(label: "com.ynd.upload.queue")
        uploadQueue.async {
            AnalitycsTracker.trackSpoofEvent(result: result, score: avgScore)
            self.uploadLastCameraPreviewImage(spoofingResult: result, score: avgScore, allReadings: allReadings)
        }
    }
}
