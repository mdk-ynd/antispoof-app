//
//  SpoofingController.swift
//  Antispoof
//
//  Created by Michael Dominic K. on 20/03/2018.
//  Copyright © 2018 YND. All rights reserved.
//

import Foundation
import Vision
import UIKit

public protocol SpoofingControllerDelegate: class {
    func spoofingControllerDidFinishProcessingFrame()
    func spoofingControllerDidChangeState(newState: SpoofingState)
    func spoofingControllerDidMarkFace(rect: CGRect, sourceImage: UIImage)
    func spoofingControllerDidGetNewReading(level: Double)
}

public enum SpoofingState {
    case showFace
    case facePresented
    case moveCloser
    case hold
    case changeLighting
    case inProgress(Double)
    case finished(Bool)

    public var majorStateIndex: Int {
        get {
            switch(self) {
            case .showFace:
                return 1
            case .facePresented:
                return 2
            case .moveCloser:
                return 3
            case .hold:
                return 4
            case .changeLighting:
                return 5
            case .inProgress:
                return 6
            case .finished:
                return 7
            }
        }
    }

    func isSameMajorState(like state: SpoofingState) -> Bool {
        return self.majorStateIndex == state.majorStateIndex
    }

    func isFinished() -> Bool {
        if isSameMajorState(like: .changeLighting) {
            return true
        }

        if isSameMajorState(like: .finished(false)) {
            return true
        }

        return false
    }
}

public class SpoofingController {
    let spoofingDetector = SpoofingDetector()
    let faceRecognizer = FaceRecognizer()
    var state = SpoofingState.showFace

    var delegate: SpoofingControllerDelegate?
    var readings = [Double]()
    var avgReading: Double {
        let sum = readings.reduce(0, +)
        let avg = sum / Double(readings.count)
        let rounded = (avg * 100.0).rounded() / 100.0
        return rounded
    }
    
    var readingsArrayString: String {
        var arrayString = "["
        for (i, reading) in self.readings.enumerated() {
            
            let rounded = (reading * 100.0).rounded() / 100.0
            if i == 0 {
                arrayString += "\(rounded)"
            } else if i == self.readings.count - 1 {
                arrayString += ", \(rounded)]"
            } else {
                arrayString += ", \(rounded)"
            }
        }
        
        return arrayString
    }
    
    let requiredRealLevel = 0.6 
    let requiredFakeLevel = 0.4
    let requiredReadingCount = 5
    let requiredFaceWidth = CGFloat(0.6)
    let lightingChangeThresh = 0.4

    var lockStateDate: Date?
    var lockStateDuration = TimeInterval(-3)

    var lockState = false

    internal func updateState(_ newState: SpoofingState) {
        // This will not be required in Swift 4.1
        // https://stackoverflow.com/questions/46782139/how-may-i-test-the-equivalency-of-enumeration-cases-with-associated-values-in-sw

        if !self.state.isSameMajorState(like: newState) {
            self.state = newState
            self.delegate?.spoofingControllerDidChangeState(newState: newState)
        }
    }

    internal func setState(_ newState: SpoofingState) {
        self.state = newState
        self.delegate?.spoofingControllerDidChangeState(newState: newState)
    }

    internal func processReadings() {
        var realHits = 0
        var fakeHits = 0

        if readings.count < 3 {
            setState(.inProgress(Double(readings.count) / Double(self.requiredReadingCount + 3)))
        }

        var i = 0
        var diff: Double = 0
        var lastReading: Double?
        for reading in readings {
            i += 1
            if i < 3 {
                continue
            }

            if reading > self.requiredRealLevel {
                realHits += 1
            } else if reading < self.requiredFakeLevel {
                fakeHits += 1
            }

            if let lastReading = lastReading {
                diff += fabs(lastReading - reading)
            }

            lastReading = reading
        }

        print("REAL: \(realHits) FAKE: \(fakeHits) diff: \(diff)")

        if diff > lightingChangeThresh {
            updateState(.changeLighting)
            self.lockState = true
            self.lockStateDate = Date(timeIntervalSinceNow: 0)
        } else if realHits >= self.requiredReadingCount  {
            updateState(.finished(true))
            self.lockState = true
            self.lockStateDate = Date(timeIntervalSinceNow: 0)
        } else if realHits < self.requiredReadingCount, fakeHits <= 2 {
            setState(.inProgress(Double(readings.count) / Double(self.requiredReadingCount + 3)))
        } else {
            updateState(.finished(false))
            self.lockStateDate = Date(timeIntervalSinceNow: 0)
            self.lockState = true
        }
    }

    public func push(pixelBuffer: CVPixelBuffer) {

        DispatchQueue.global().async {

            // If the state is locked and if the current state is not successfully finished
            // then reset state after a certain period of time has elapsed.
            if self.lockState, let lockStateDate = self.lockStateDate, self.state.isFinished() {
                let duration = lockStateDate.timeIntervalSinceNow
                if duration < self.lockStateDuration {
                    self.lockStateDate = nil
                    self.lockState = false
                    self.readings = [Double]()
                    self.delegate?.spoofingControllerDidFinishProcessingFrame()
                    self.updateState(.showFace)
                }
            }

            let ciImage = CIImage(cvPixelBuffer: pixelBuffer)
            let tempContext = CIContext(options: nil)
            let width = CVPixelBufferGetWidth(pixelBuffer)
            let height = CVPixelBufferGetHeight(pixelBuffer)
            let rect = CGRect(x: 0, y: 0, width: width, height: height)
            let cgImage = tempContext.createCGImage(ciImage, from: rect)
            let uiImage = UIImage(cgImage: cgImage!)

            self.faceRecognizer.recognizeFaces(for: uiImage, completion: { (faces) in
                if let firstFace = faces.first {
                    
                    
                    if self.lockState {
                        self.delegate?.spoofingControllerDidFinishProcessingFrame()
                        return
                    }
                    
                    self.delegate?.spoofingControllerDidMarkFace(rect: firstFace.rect, sourceImage: uiImage)
                    
                    var widthFactor = firstFace.image.size.width / CGFloat(width)

                    // If we're already in progress we giv e a little hint to the required width
                    // factor avoid on/off oscillations

                    switch self.state {
                    case .inProgress(_):
                        widthFactor += 0.2
                    default:
                        break
                    }

                    if widthFactor < self.requiredFaceWidth {
                        if self.state.isSameMajorState(like: .facePresented) {
                            self.updateState(.moveCloser)
                        } else {
                            self.setState(.facePresented)
                        }

                        self.delegate?.spoofingControllerDidFinishProcessingFrame()
                        return
                    }

                    // This is a quick state setter to have a the "hold" message display as
                    // soon as possible
                    switch self.state {
                    case .showFace, .moveCloser:
                        self.setState(.hold)
                    default:
                        break
                    }

                    self.spoofingDetector.detectSpoofing(face: firstFace.image, completion: { (level) in
                        self.delegate?.spoofingControllerDidGetNewReading(level: level)
                        self.readings.append(level)
                        self.processReadings()
                        self.delegate?.spoofingControllerDidFinishProcessingFrame()
                    })
                } else {
                    // There are no faces. Instantly mark as processed.
                    self.lockStateDate = nil
                    self.lockState = false
                    self.readings = [Double]()
                    self.delegate?.spoofingControllerDidFinishProcessingFrame()
                    self.updateState(.showFace)
                }
            })
        }
    }
}
