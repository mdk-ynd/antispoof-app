//
//  SpoofingDetection.swift
//  FaceCapture
//
//  Created by Michael Dominic K. on 14/02/2018.
//  Copyright © 2018 Przemyslaw Probola. All rights reserved.
//

import Foundation
import UIKit
import CoreML
import Vision

class SpoofingDetector {

    enum Provider {
        case visionProcessing
        case standardProcessing
    }

    typealias PredictionCompletion = (Double) -> Void

    let provider: Provider
    let standardModel: SpoofingStandard?
    var faceCount = 0

    var mlRequest: VNCoreMLRequest? = nil
    var visionCompletion: PredictionCompletion? = nil

    var lastDetectionImage = UIImage()
    
    init(using provider: Provider = .standardProcessing) {
        self.provider = provider

        switch(provider) {
        case .standardProcessing:
            self.standardModel = SpoofingStandard()

            break

        case .visionProcessing:
            self.standardModel = nil
            break;
        }
    }

    private func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage? {
        let size = image.size

        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height

        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
        }

        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: targetSize.width, height: targetSize.height)

        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(targetSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage
    }

    private func dumpToDocuments(image: UIImage) throws {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]

        let fileName = "face-\(self.faceCount).png"
        let fullPath = "\(documentsPath)/\(fileName)"
        self.faceCount += 1
        try UIImagePNGRepresentation(image)?.write(to: URL(fileURLWithPath: fullPath))
    }

    private func createInputArray(from image: UIImage) -> MLMultiArray {
        let input = try! MLMultiArray(shape: [224, 224, 3], dataType: .double)

        guard
            let data = image.cgImage?.dataProvider?.data,
            let bytes = CFDataGetBytePtr(data) else {
                print("Failed to extract image data bytes!")
                return input
        }

        for y in 0..<Int(image.size.width) {
            for x in 0..<Int(image.size.height) {
                let index = (((y * Int(image.size.width)) + x) * 4)
                let r = bytes[index + 0]
                let g = bytes[index + 1]
                let b = bytes[index + 2]

                input[[NSNumber(value: y) , NSNumber(value: x), NSNumber(value: 0)]] =
                    NSNumber(value: (Double(r) / 255.0))
                input[[NSNumber(value: y) , NSNumber(value: x), NSNumber(value: 1)]] =
                    NSNumber(value: (Double(g) / 255.0))
                input[[NSNumber(value: y) , NSNumber(value: x), NSNumber(value: 2)]] =
                    NSNumber(value: (Double(b) / 255.0))
            }
        }

        return input
    }

    func visionRequestDidComplete(request: VNRequest, error: Error?) {
        guard let observations = request.results as? [VNCoreMLFeatureValueObservation],
            let features = observations.first?.featureValue.multiArrayValue else {
                return
        }

        let result = features[[NSNumber(value: 0) , NSNumber(value: 0)]].doubleValue
        self.visionCompletion?(result)
    }

    private func detectSpoofingUsingStandard(face: UIImage, completion: PredictionCompletion) {
        guard let model = self.standardModel else {
            print("Standard processing mode not available!")
            return
        }

        guard let scaledFace = resizeImage(image: face, targetSize: CGSize(width: 224, height: 224)) else {
            print("Failed to resize incoming image!")
            return
        }

        // try! dumpToDocuments(image: scaledFace)
        lastDetectionImage = scaledFace
        let input = createInputArray(from: scaledFace)
        let output = try! model.prediction(_0: input)
        let result = output._75[[NSNumber(value: 0) , NSNumber(value: 0)]].doubleValue

        completion(result)
    }

    func detectSpoofing(face: UIImage, completion: @escaping PredictionCompletion) {
        switch(self.provider) {
        case .standardProcessing:
            detectSpoofingUsingStandard(face: face, completion: completion)
            break
        case .visionProcessing:
            //detectSpoofingUsingVision(face: face, completion: completion)
            break
        }
    }
}


//private func detectSpoofingUsingVision(face: UIImage, completion: @escaping PredictionCompletion) {
//    guard let model = self.visionModel else {
//        print("Vision processing mode not available!")
//        return
//    }
//
//    if mlRequest == nil {
//        self.mlRequest = try! VNCoreMLRequest(model: VNCoreMLModel(for: model.model), completionHandler: visionRequestDidComplete)
//        self.mlRequest?.imageCropAndScaleOption = .scaleFill
//    }
//
//    guard let request = mlRequest else {
//        print("Vision not setup correctly!")
//        return
//    }
//
//    guard let scaledFace = resizeImage(image: face, targetSize: CGSize(width: 224, height: 224  )) else {
//        print("Failed to resize incoming image!")
//        return
//    }
//
//    self.visionCompletion = completion
//
//    // try! dumpToDocuments(image: scaledFace)
//
//    if let cgImage = scaledFace.cgImage {
//        let handler = VNImageRequestHandler(cgImage: cgImage)
//        try! handler.perform([request])
//    } else {
//        print("Failed to obtain CGImage from image!")
//    }
//}
