//
//  FaceRecognizer.swift
//  Antispoof
//
//  Created by Michael Dominic K. on 20/03/2018.
//  Copyright © 2018 YND. All rights reserved.
//

import Foundation
import Vision
import UIKit

struct FaceResult {
    let image: UIImage
    let rect: CGRect
}

final class FaceRecognizer {

    private func faceLandmarkRegions(from observation: VNFaceObservation) -> [VNFaceLandmarkRegion2D] {
        guard let landmarks = observation.landmarks else {
            return []
        }

        var landmarkRegions: [VNFaceLandmarkRegion2D] = []
        if let faceContour = landmarks.faceContour {
            landmarkRegions.append(faceContour)
        }
        if let leftEye = landmarks.leftEye {
            landmarkRegions.append(leftEye)
        }
        if let rightEye = landmarks.rightEye {
            landmarkRegions.append(rightEye)
        }
        if let nose = landmarks.nose {
            landmarkRegions.append(nose)
        }
        if let noseCrest = landmarks.noseCrest {
            landmarkRegions.append(noseCrest)
        }
        if let medianLine = landmarks.medianLine {
            landmarkRegions.append(medianLine)
        }
        if let outerLips = landmarks.outerLips {
            landmarkRegions.append(outerLips)
        }
        /*
         if let leftEyebrow = landmarks.leftEyebrow {
         landmarkRegions.append(leftEyebrow)
         }
         if let rightEyebrow = landmarks.rightEyebrow {
         landmarkRegions.append(rightEyebrow)
         }

         if let innerLips = landmarks.innerLips {
         landmarkRegions.append(innerLips)
         }
         if let leftPupil = landmarks.leftPupil {
         landmarkRegions.append(leftPupil)
         }
         if let rightPupil = landmarks.rightPupil {
         landmarkRegions.append(rightPupil)
         }
         */
        return landmarkRegions
    }

    private func cropImage(_ source: UIImage, boundingRect: CGRect) -> UIImage? {

        guard let cropped = source.cgImage?.cropping(to: boundingRect) else {
            return nil
        }
        return UIImage(cgImage: cropped)
    }

    private func calculateNormalRect(with normalized: CGRect, source: UIImage) -> CGRect {
        
        var x = normalized.origin.x * source.size.width
        var y = normalized.origin.y * source.size.height
        let rectWidth = source.size.width * normalized.size.width
        let rectHeight = source.size.height * normalized.size.height

        y = source.size.height - y - rectHeight
        
        if x < 0 {
            x = 0
        }
        if y < 0 {
            y = 0
        }
        let normalRect = CGRect(x: x, y: y, width: rectWidth, height: rectHeight)
        
        return normalRect
    }
    
    func recognizeFaces(for source: UIImage, completion: @escaping (_ faces: [FaceResult]) -> Void) {

        let orginalImage = source

        let detectFaceRequest = VNDetectFaceLandmarksRequest { (request, error) in
            var croppedFaces = [FaceResult]()
            if let error = error {
                print(error.localizedDescription)
                return
            }

            if let results = request.results as? [VNFaceObservation] {
                for faceObservation in results {

                    let boundingRect = faceObservation.boundingBox
                    let conf = faceObservation.confidence
                    let uuid = faceObservation.uuid
                    print("Face uuid = \(uuid) confidence = \(conf)")

                    let normalRect = self.calculateNormalRect(with: boundingRect, source: orginalImage)
                    if let cropped = self.cropImage(orginalImage, boundingRect: normalRect) {
                        croppedFaces.append(FaceResult(image: cropped, rect: normalRect))
                    }
                }
            }

            completion(croppedFaces)
        }

        let vnImage = VNImageRequestHandler(cgImage: source.cgImage!, options: [:])
        try? vnImage.perform([detectFaceRequest])
    }
}
